The agent vocabulary describes networks of people, organizations and networks, constructed using a simple but powerful model of agents and their relationships.

### Agents

Agents can be individual persons or organizations. Organizations include formal or informal organizations of all kinds. (The concept of "Agent" will in the future probably include "ecological agents", as we explore with others how to use REA for ecological accounting to combat climate change. The concept of "Agent" could also in the future include software agents, but this is controversial.)

In Valueflows, we are talking about economic agents, agents who can create or exchange value, and make agreements with each other - who have economic agency.  But we want to re-use existing vocabularies for commonly defined things (including foaf:Agent, foaf:Person, org:Organization), so we have elected to use those as much as possible here, even though they are sometimes more broadly defined.  But also, the broader definitions will help map and integrate technologies across the web.

If people want to define types of organizations (like cooperative, corporation, network, community, etc.) we provide a classification property which people can define as they wish.

### Agent Relationships

Agent relationships have many nuances, thus VF provides the ability to define one's own kinds of relationships.  For example people might "participate" with an organization by means of agreeing to terms and conditions.  Or people might have more active "membership" in a group or organization.  Or people might consider themselves members but want a more independently flavored term such as "affiliates".

A relationship can be direct, like "steward", or more like a role, for example "grower" or "harvester" for a food network.

Relationships can also include roles like "sub-organization" or "trading partner".

There are a number of useful Properties in existing vocabularies that can be used. Or people can create their own as needed.

Relationships have direction: For example, in "Michael is a member of Enspiral", Michael is the subject and Enspiral is the object.  In this case the inverse is also valid, "Enspiral has member Michael". In VF, we consider this to be one relationship.  One directional relationships like "follows" are also supported.

Relationships can be in a [scope](scoping.md) (or not): For example, "Kathy is mentor of Sam, in the scope of Enspiral."

### Agent Philosophies

We also want to acknowledge that some people prefer to think of themselves as independent and decentralized agents who interact in different places in the economy as individuals, and some people think of themselves more as members of different groups and networks and communities and interact more in the context of those groups and networks and communities.  Many experiments are going on as people strive towards another economy.  We want to support all these experiments, so want to support both of these ways of thinking and organizing ourselves.  The agent vocabulary is very flexible, and will support these as well as current conventional structures.

So, if people want to form a group that has agency as a group, fine.  If people want to consider that their group does not have agency as a group, also fine.  Not all groups, and especially not all networks, will be economic Agents in Valueflows. That depends on the agreement of the people in the group, and what the group needs to do as-a-group. For example, does the group need to make agreements as-a-group with other groups? Or exchange resources with other agents as-a-group?  Note that within the vocabulary, network formations will appear, as agents have economic interactions with each other in the world.  This does not mean that the network is necessarily a Valueflows Agent, but it could be, if the participants want.
