Read further for:

**[a web-based formatted view](https://w3id.org/lode/owlapi/https://lab.allmende.io/valueflows/valueflows/-/raw/master/release-doc-in-process/all_vf.TTL)** ([alternative](/specification/all_vf.html)) of the Valueflows class and property definitions.

**[a web view of the source "system of record" turtle (ttl) file](https://lab.allmende.io/valueflows/valueflows/-/blob/master/release-doc-in-process/all_vf.TTL)** ([download](https://lab.allmende.io/valueflows/valueflows/-/raw/master/release-doc-in-process/all_vf.TTL?inline=false)) for all Valueflows class and property definitions.

*Note*: Because of the way the semantic web works, both of these represent only the Valueflows namespace.  See [Other Namespaces](external-terms.md) for details on the non-Valueflows namespace elements. For complete representations for what is needed for a Valueflows based core economic vocabulary, see [Complete Diagram](uml.md) or [GraphQL Reference](graphql.md).
